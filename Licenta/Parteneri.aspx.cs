﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Xml.Linq;
using System.IO;
using System.ComponentModel.DataAnnotations;

namespace Licenta
{
    public partial class Reduceri : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ShowReduceri();
        }

        SqlConnection Con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=""C:\Users\USER\Desktop\test Selenium\Licenta\FarmacieTb.MDF"";Integrated Security=True;Connect Timeout=30");
        private void ShowReduceri()
        {
            Con.Open();
            string Query = "select * from FarmaPart";
            SqlDataAdapter sda = new SqlDataAdapter(Query, Con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            sda.Fill(ds);
            RedcGV.DataSource = ds.Tables[0];
            RedcGV.DataBind();
            Con.Close();
        }

        private void DelRed()
        {
            if (Partener.Value == "" || Mediu.SelectedIndex == -1 || NrTel.Value == "" || Adresa.Value == "")
            {
                 ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Selectează o farmacie parteneră pentru a o ȘTERGE');", true);
                // eroare
            }
            else

            {
                try
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("Delete from FarmaPart where IdPart=@Pk", Con);
                    cmd.Parameters.AddWithValue("@Pk", RedcGV.SelectedRow.Cells[1].Text);
                    cmd.ExecuteNonQuery();
                    Con.Close();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('S-a șters o farmacie parteneră');", true);
                    ShowReduceri();
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }
        private void EditRed()
        {
            if (Partener.Value == "" || Parola.Value == "" || Adresa.Value == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Selectează și editează un câmp');", true);
                // eroare
            }
            else

            {
                try
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("update FarmaPart set Partener=@PP, Mediu=@PM, DI=@DI, NrTel=@RNT, Adresa=@RA, Parola=@RP where IdPart=@Pk ", Con);
                    cmd.Parameters.AddWithValue("@PP", Partener.Value);
                    cmd.Parameters.AddWithValue("@DI", DI.Value);
                    cmd.Parameters.AddWithValue("@RNT", NrTel.Value);
                    cmd.Parameters.AddWithValue("@RA", Adresa.Value);
                    cmd.Parameters.AddWithValue("@PM", Mediu.SelectedItem.ToString());
                    cmd.Parameters.AddWithValue("@RP", Parola.Value);
                    cmd.Parameters.AddWithValue("@Pk", RedcGV.SelectedRow.Cells[1].Text);
                    cmd.ExecuteNonQuery();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('S-au actualizat partenerii');", true);
                  Con.Close();  ShowReduceri();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        private void InsertRed()
        {
            if (Partener.Value == "" || Mediu.SelectedIndex == -1 || NrTel.Value == "" || Adresa.Value == "")
            { 
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Eroare');", true);
            }
            else

            {
                try
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("insert into FarmaPart(Partener,DI,NrTel,Adresa,Mediu,Parola) values(@PP, @DI,@RNT,@RA,@RG,@RP)", Con);
                    cmd.Parameters.AddWithValue("@PP", Partener.Value);
                    cmd.Parameters.AddWithValue("@DI", DI.Value);
                    cmd.Parameters.AddWithValue("@RNT", NrTel.Value);
                    cmd.Parameters.AddWithValue("@RA", Adresa.Value);
                    cmd.Parameters.AddWithValue("@RG", Mediu.SelectedItem.ToString());
                    cmd.Parameters.AddWithValue("@RP", Parola.Value);
                    
                    cmd.ExecuteNonQuery();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('S-au adăugat reduceri);", true);
                    Con.Close();
                    ShowReduceri();
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }
        int k = 0;
        protected void RedcGV_SelectedIndexChanged(object sender, EventArgs e)
        {
            Partener.Value = RedcGV.SelectedRow.Cells[2].Text;
            Mediu.DataTextFormatString = RedcGV.SelectedRow.Cells[3].Text;
            DI.Value = RedcGV.SelectedRow.Cells[4].Text;
            NrTel.Value = RedcGV.SelectedRow.Cells[4].Text;
            Adresa.Value = RedcGV.SelectedRow.Cells[5].Text;
            Parola.Value = RedcGV.SelectedRow.Cells[7].Text;
            if (Partener.Value == "")
            {
                k = 0;
            }
            else
            {
                k = Convert.ToInt32(RedcGV.SelectedRow.Cells[1].Text);
            }
        }

       

        protected void butadd_Click(object sender, EventArgs e)
        {
            InsertRed();
        }

        protected void Bdel_Click(object sender, EventArgs e)
        {
            DelRed();
        }

        protected void Bed_Click(object sender, EventArgs e)
        {
            EditRed();
        }

        protected void Mediu_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        

        protected void BEmail_Click(object sender, EventArgs e)
        {
            try
            {
                // Adresa de email a partenerului
                string adrEmailFarmaPartenera = Parola.Value;

              if (string.IsNullOrWhiteSpace(adrEmailFarmaPartenera))
                {
    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Selectează un partener pentru a trimite email.');", true);
                    return;
                }

                if (!adrEmailFarmaPartenera.Contains(".") || !adrEmailFarmaPartenera.Contains("@"))
                {
    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Adresa de email a partenerului este invalidă.');", true);
                    return;
                }

                string gmailUrl = "https://mail.google.com/mail/?view=cm&to=" + adrEmailFarmaPartenera;
                Response.Redirect(gmailUrl);
            }
            catch (Exception ex)
            {
 ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Eroare la deschiderea aplicației: " + ex.Message + "');", true);
            }
        }
    }
   
}