﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Licenta
{
    public partial class AdminStart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btcont_Click(object sender, EventArgs e)
        {
            Response.Redirect("Inreg.aspx");
        }

        protected void MeniuPrin_Click(object sender, EventArgs e)
        {
            Response.Redirect("Home.aspx");
        }
    }
}