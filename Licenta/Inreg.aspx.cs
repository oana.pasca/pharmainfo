﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Web;
using System.Web.Http.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace Licenta.Admin
{
    public partial class Inreg : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)


        {


            ShowInreg();
            

        }

        SqlConnection Con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=""C:\Users\USER\Desktop\test Selenium\Licenta\FarmacieTb.MDF"";Integrated Security=True;Connect Timeout=30");
        private void ShowInreg()
        {
            Con.Open();
            string Query = "select * from FarmacistiUtil";
            SqlDataAdapter sda = new SqlDataAdapter(Query, Con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            sda.Fill(ds);
            UtilList.DataSource = ds.Tables[0];
            UtilList.DataBind();
            Con.Close();
        }



        protected void btcont_Click(object sender, EventArgs e)
        {
            InsertUtil();

        }

        private void InsertUtil()
        {
            if (Adresa.Value == "" || Parola.Value == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Pentru a crea un cont trebuie să completezi toate câmpurile');", true);
            }
            else

            {
                try
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("insert into FarmacistiUtil(Adresa,Parola, Nume, IDangajare, NrTelefon) values(@A, @P,@N,@ID,@NT)", Con);
                    cmd.Parameters.AddWithValue("@A", Adresa.Value);
                    cmd.Parameters.AddWithValue("@P", Parola.Value);
                    cmd.Parameters.AddWithValue("@N", Nume.Value);
                    cmd.Parameters.AddWithValue("@ID", IDangajare.Value);
                    cmd.Parameters.AddWithValue("@NT", NrTelefon.Value);
                    cmd.ExecuteNonQuery();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('S-a creat cont);", true);
                    Con.Close();
                    ShowInreg();
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }



        protected void btned_Click(object sender, EventArgs e)
        {
            EditCont();
        }

        protected void btndel_Click(object sender, EventArgs e)
        {
            if (Adresa.Value == "" || Nume.Value == "" || NrTelefon.Value == "" || IDangajare.Value == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Selectează un farmacist pentru a-l ȘTERGE');", true);
                // eroare

            }
            else

            {
                try
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("Delete from FarmacistiUtil where FarId=@FID", Con);
                    cmd.Parameters.AddWithValue("@FID", UtilList.SelectedRow.Cells[1].Text);
                    cmd.ExecuteNonQuery();
                    Con.Close();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('S-a șters un utilizator');", true);
                    ShowInreg();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        private void EditCont()
        {
            if (Adresa.Value == "" || Nume.Value == "" || IDangajare.Value == "" || NrTelefon.Value == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Eroare');", true);
                // eroare
            }
            else

            {
                try
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("update FarmacistiUtil set Adresa=@A, Parola=@P, Nume=@Nu, IDangajare=@IA, NrTelefon=@Nr where FarId=@FID", Con);
                    cmd.Parameters.AddWithValue("@A", Adresa.Value);
                    cmd.Parameters.AddWithValue("@P", Parola.Value);
                    cmd.Parameters.AddWithValue("@Nu", Nume.Value);
                    cmd.Parameters.AddWithValue("@IA", IDangajare.Value);
                    cmd.Parameters.AddWithValue("@Nr", NrTelefon.Value);
                    cmd.Parameters.AddWithValue("@FID", UtilList.SelectedRow.Cells[1].Text);
                    cmd.ExecuteNonQuery();
                    Con.Close();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Actualizare reușită!');", true);
                    ShowInreg();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        protected void Grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Grid View Clicked');", true);
        }
        int k = 0;

        protected void UtilList_SelectedIndexChanged(object sender, EventArgs e)
        {
            Adresa.Value = UtilList.SelectedRow.Cells[2].Text;
            Parola.Value = UtilList.SelectedRow.Cells[3].Text;
            Nume.Value = UtilList.SelectedRow.Cells[4].Text;
            IDangajare.Value = UtilList.SelectedRow.Cells[5].Text;
            NrTelefon.Value = UtilList.SelectedRow.Cells[6].Text;
            if (Adresa.Value == " ")
            {
                k = 0;
            }
            else
            {
                k = Convert.ToInt32(UtilList.SelectedRow.Cells[1].Text);
            }

        }
        
    }
}


    
    


