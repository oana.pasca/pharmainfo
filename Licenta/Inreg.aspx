﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Inreg.aspx.cs" Inherits="Licenta.Admin.Inreg" %>


<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Creare cont</title>
   <link rel="stylesheet" href="Inregistrare.css"/>

     <style>
          
            .table-condesed tr th{
                border-style:outset;
                border-width: 2px;
            
            }
            .table-condesed, .table-condesed tr td{
                border: 0px solid #000;
            }
            tr:nth-child(even){
                
            }
            tr:nth-child(odd){
                
            }
        
            input[type=text], select {
    width: 100%;
    padding: 0 5px;
    height: 40px;
    font-size: 13px;
    border: 2px;
    background: none;
    
}
              input[type=number], select {
    width: 100%;
    padding: 0 5px;
    height: 40px;
    font-size: 13px;
    border: 2px;
    background: none;
    
}

input[type=email], select {
    width: 100%;
    padding: 0 5px;
    height: 40px;
    font-size: 13px;
    border: 2px;
    background: none;
}

input[type=password], select {
    width: 100%;
    padding: 0 5px;
    height: 40px;
    font-size: 13px;
    border: 2px;
    background: none;
}

#UtilList {
   
    left: 0%;
    
    height: 188px;
    font-family: 'Times New Roman', Times, serif;

}
        </style></head>
<body>
      
    <div class="centru">
       <h1>Creare cont - PharmaInfo</h1>
       <form runat="server" method="post">
           <div id="id">

                                  
                       <center><input type="email" name="" id="Adresa" placeholder="Adresa email" runat="server" autocomplete="off" >
                       <input type="password" name="" id="Parola" placeholder="Parola" runat="server" autocomplete="off" >
                       <input type="text" name="" id="Nume" placeholder="Nume și prenume" runat="server" autocomplete="off">
                       <input type="number" name="" id="IDangajare" placeholder="ID angajare" runat="server" autocomplete="off">
                       <input type="text" name="" id="NrTelefon" placeholder="Număr telefon" runat="server" autocomplete="off">
                       </center>
               <br><br />
                   <asp:Button runat="server" ID="btcont" Text="Creare cont" OnClick="btcont_Click" style="height: 27px;
   width: 30%;
   border: 0px solid;
   background:#1aa233;
   border-radius: 25px;
   font-size:14px;
   color: #ffffff;outline: none;"></asp:Button><asp:Button runat="server" Text="Editează" ID="btned" Style="height: 27px; width: 30%; border: 0px solid; background: #1aa233; border-radius: 25px; font-size: 14px; color: #ffffff; outline: none;" OnClick="btned_Click"></asp:Button><asp:Button runat="server" Text="Șterge" ID="btndel" OnClick="btndel_Click" Style="height: 27px; width: 30%; border: 0px solid; background: #1aa233; border-radius: 25px; font-size: 14px; color: #ffffff; outline: none;" ></asp:Button>

               </div>
                
          <br><br />
 
<left><div id="GridMV">
    <asp:GridView ID="UtilList" runat="server" CssClass="table table-condensed table-hover"
        OnSelectedIndexChanged="UtilList_SelectedIndexChanged"
        AutoGenerateSelectButton="True" DataKeyNames="FarId" AutoPostBack="true" >
    </asp:GridView></left>





</div>
       </form>
       </div>
     
          
           
           
  </div>
</body>
</html>
