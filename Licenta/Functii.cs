﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;
using System.Linq;
using System.Web;

namespace Licenta
{
    public class Functii
    {  
        private SqlConnection Con;
        private SqlCommand cmd;
        private DataTable dt;
        private SqlDataAdapter sda;
        private string ConString;

        public Functii()
        {
            ConString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=""C:\Users\USER\Desktop\test Selenium\Licenta\FarmacieTb.MDF"";Integrated Security=True;Connect Timeout=30";
            Con = new SqlConnection(ConString);
            cmd = new SqlCommand();
            cmd.Connection = Con;

        }
        public DataTable GetData(string Query)
        {
            dt = new DataTable();
            sda = new SqlDataAdapter(Query, ConString);
            sda.Fill(dt);
            return dt;
        }

        public int SetData(string Query)
        {
            int ct = 0;
            if(Con.State == ConnectionState.Closed)
                 {
                Con.Open();

            }
            cmd.CommandText = Query;
            ct = cmd.ExecuteNonQuery();
            Con.Close();
            return ct;
        }
    }
}