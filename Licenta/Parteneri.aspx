﻿ <%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Parteneri.aspx.cs" Inherits="Licenta.Reduceri" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<html lane="en">
    <head>

       <title>Parteneri</title>

        <link rel="stylesheet" href="Farmacie.css">
        
        <style>
            .table-condesed tr th{
                border-style:outset;
                border-width: 5px;
                border-color: rgb(255, 216, 0);
            }
            .table-condesed, .table-condesed tr td{
                border: 0px solid #000;
            }
            tr:nth-child(even){
                background:rgb(255, 216, 0);
            }
            tr:nth-child(odd){
                background:rgb(235, 205, 36);
            }
        </style>
    </head>
    <body>

        <div id="BigMed">
             
           <div id="id">
              <h2><center>Farmacii partenere - PharmaInfo</center></h2>   <br><br>       
 
               <center><input type="text" name="" id="Partener" placeholder="Farmacie" runat="server">

          <asp:DropDownList ID="Mediu" runat="server" SelectedIndexChanged="Mediu_SelectedChanged" AutoPostBack="True" OnSelectedIndexChanged="Mediu_SelectedIndexChanged">
                    <asp:ListItem Text="Rural" Value="Rural"></asp:ListItem>
                    <asp:ListItem Text="Urban" Value="Urban"></asp:ListItem>
                    </asp:DropDownList>

                <input type="date" name="" id="DI" runat="server">
                <input type="tel" name="" id="NrTel" placeholder="Nr.Telefon" runat="server">
                <input type="text" name="" id="Adresa" placeholder="Adresă" runat="server">
                <input type="text" name="" id="Parola" placeholder="Adresa de email" runat="server">
                          
                    
                 
               </center>
                                            
               <center>  
                   <asp:Button ID="Badd" runat="server" Text="Adaugă" Style="background-color: olivedrab; width: 70px; height: 30px; border-radius: 9px; margin: 45px 0" OnClick="butadd_Click"></asp:Button>                                    
                   <asp:Button runat="server" Text="Editează" Style="background-color: olivedrab; width: 70px; height: 30px; border-radius: 9px; margin: 45px 0" OnClick="Bed_Click" ></asp:Button>  
                  </asp:Button><asp:Button runat="server" ID="Bdel" Text="Șterge" Style="background-color: olivedrab; width: 70px; height: 30px; border-radius: 9px; margin: 45px 0" OnClick="Bdel_Click"></asp:Button>  
                                   </asp:Button><asp:Button runat="server" ID="BEmail" Text="Trimite email" Style="background-color: olivedrab; width: 100px; height: 30px; border-radius: 9px; margin: 45px 0" OnClick="BEmail_Click"></asp:Button>  

               
                <div id="RedGV">
                    <asp:GridView ID="RedcGV" runat="server" Width="934px" CssClass="table table-condensed table-hover" OnSelectedIndexChanged="RedcGV_SelectedIndexChanged" AutoGenerateSelectButton="True">
                    </asp:GridView>

                </div>
            </div>  
           
               </body>
    </html>

</asp:Content>
