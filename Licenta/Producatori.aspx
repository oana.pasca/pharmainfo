﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Producatori.aspx.cs" Inherits="Licenta.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <html lane="en">
    <head>

       <title>InfoPharma</title>

        <link rel="stylesheet" href="Farmacie.css">
        
        <style>
            .table-condesed tr th{
                border-style:outset;
                border-width: 5px;
                border-color: rgb(255, 216, 0);
            }
            .table-condesed, .table-condesed tr td{
                border: 0px solid #000;
            }
            tr:nth-child(even){
                background:rgb(255, 216, 0);
            }
            tr:nth-child(odd){
                background:rgb(235, 205, 36);
            }
        </style>
    </head>
    <body>

        <div id="BigMed">
             
           <div id="id"><h2><center>Producători</center></h2>          
                <div id="Bloc1"><br><br>
               <center><input type="text" name="" id="PNume" placeholder="Nume Producător" runat="server">
                <input type="text" name="" id="PAdr" placeholder="Adresa" runat="server">
                <input type="text" name="" id="PNrTel" placeholder="Nr. telefon" runat="server">
                <input type="date" name="" id="PData" placeholder="Data" runat="server"><br /></ br>
 
               </center></div>
               
               <div id="BtDiv"><center>
        <asp:Button runat="server" Text="Adaugă" OnClick="butAd" style="background-color:olivedrab;width:70px;height:30px;border-radius:9px;margin:45px 0"></asp:Button>
        <asp:Button runat="server" Text="Editează" OnClick="butEd" style="background-color:olivedrab;width:70px;height:30px;border-radius:9px;margin:45px 0"></asp:Button>
        <asp:Button runat="server" Text="Șterge" OnClick="butDel" style="background-color:olivedrab;width:70px;height:30px;border-radius:9px;margin:45px 0" > </asp:Button>
                   <asp:Button runat="server" Text="Trimite email" OnClick="butEmail" style="background-color:olivedrab;width:100px;height:30px;border-radius:9px;margin:45px 0" > </asp:Button>
        </center> </div>
        
        <div id="DivGV"> <center>
            <asp:GridView ID="DivGV" runat="server" Width="934px" CssClass="table table-condensed table-hover" OnSelectedIndexChanged="DivGV_SelectedIndexChanged" AutoGenerateSelectButton="True"></asp:GridView>
               </center> </div>

               
               
               
            </div>  
           </div>
               </body>
    </html>
</asp:Content>
