﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Configuration;

namespace Licenta
{
    public partial class Contact : Page
    {
        SqlConnection Con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=""C:\Users\USER\Desktop\test Selenium\Licenta\FarmacieTb.MDF"";Integrated Security=True;Connect Timeout=30");
        private void ShowContact()
        {
            Con.Open();
            string Query = "select * from ProducatorTb";
            SqlDataAdapter sda = new SqlDataAdapter(Query, Con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            sda.Fill(ds);
            DivGV.DataSource = ds.Tables[0];
            DivGV.DataBind();
            Con.Close();
        }
            protected void Page_Load(object sender, EventArgs e)
        {
            ShowContact();
        }

        private void InsertProd()
        {
            if (PNume.Value == "" || PNrTel.Value == "" || PAdr.Value == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Eroare');", true);
                // eroare
            }
            else

            {
                try
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("insert into ProducatorTB(PNume,PAdr,PNrTel,PData) values(@PN, @PA,@PNT,@PD)", Con);
                    cmd.Parameters.AddWithValue("@PN", PNume.Value);
                    cmd.Parameters.AddWithValue("@PA", PAdr.Value);
                    cmd.Parameters.AddWithValue("@PNT", PNrTel.Value);
                    cmd.Parameters.AddWithValue("@PD", PData.Value);              
                    cmd.ExecuteNonQuery();
                    Con.Close();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('S-au adăugat producători');", true);
                    ShowContact();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        
        private void EditPr()
        {
            if (PNume.Value == "" || PNrTel.Value == "" || PAdr.Value == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Lipsesc informații noi');", true);
                // eroare
            }
            else

            { 
                try
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("update ProducatorTB set PNume=@PN,PAdr=@PA,PNrTel=@PNT,PData=@PD where ProdId=@Pk", Con);
                    cmd.Parameters.AddWithValue("@PN", PNume.Value);
                    cmd.Parameters.AddWithValue("@PA", PAdr.Value);
                    cmd.Parameters.AddWithValue("@PNT", PNrTel.Value);
                    cmd.Parameters.AddWithValue("@PD", PData.Value);
                    cmd.Parameters.AddWithValue("@Pk", DivGV.SelectedRow.Cells[1].Text);
                    cmd.ExecuteNonQuery();                   
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('S-au actualizat producători');", true);
                    Con.Close();
                    ShowContact();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        private void DelProd()
        {
            if (DivGV.SelectedRow.Cells[1].Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Selectează un producător pentru a-l ȘTERGE');", true);
                // eroare
            }
            else

            {
                try
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("Delete from ProducatorTB where ProdId=@Mk", Con);
                    cmd.Parameters.AddWithValue("@Mk", DivGV.SelectedRow.Cells[1].Text);
                    cmd.ExecuteNonQuery();
                    Con.Close();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('S-au șters producători');", true);
                    ShowContact();
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }
        protected void butAd(object sender, EventArgs e)
        {
            InsertProd();
        }

        protected void butDel(object sender, EventArgs e)
        {
            DelProd(); 
        }

        protected void Contact_Load(object sender, EventArgs e)
        {
            ShowContact();
        }

        protected void butEd(object sender, EventArgs e)
        {
            EditPr();
        }
        int k = 0;
        protected void DivGV_SelectedIndexChanged(object sender, EventArgs e)
        {
            PNume.Value = DivGV.SelectedRow.Cells[2].Text;
            PAdr.Value = DivGV.SelectedRow.Cells[3].Text;
            PNrTel.Value = DivGV.SelectedRow.Cells[4].Text;
            PData.Value = DivGV.SelectedRow.Cells[5].Text;
                if(PNume.Value=="")
            {
                k = 0;
            }
                else
            {
                k = Convert.ToInt32(DivGV.SelectedRow.Cells[1].Text);
            }
        }

        protected void butEmail(object sender, EventArgs e)
        {
            try
            {
                // Adresa de email a partenerului
                string adrProducatoriEmail = PAdr.Value;

                if (string.IsNullOrWhiteSpace(adrProducatoriEmail))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Selectează un producător pentru a trimite email.');", true);
                    return;
                }

                if (!adrProducatoriEmail.Contains(".") || !adrProducatoriEmail.Contains("@"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Adresa de email a producătorului este invalidă.');", true);
                    return;
                }

                string gmailUrl = "https://mail.google.com/mail/?view=cm&to=" + adrProducatoriEmail;
                Response.Redirect(gmailUrl);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Eroare la deschiderea aplicației: " + ex.Message + "');", true);
            }
        }
    }
}

