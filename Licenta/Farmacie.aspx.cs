﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Drawing.Printing;
using System.Drawing;
using System.Collections.Generic;
using System.Web.DynamicData;
using System.Xml.Linq;


namespace Licenta
{
    public partial class About : Page
    {

        SqlConnection Con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=""C:\Users\USER\Desktop\test Selenium\Licenta\FarmacieTb.MDF"";Integrated Security=True;Connect Timeout=30");
        private void ShowFarmacie()
        {
            Con.Open();
            string Query = "select * from FarmacieTb";
            SqlDataAdapter sda = new SqlDataAdapter(Query, Con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            sda.Fill(ds);
            FarmaGV.DataSource = ds.Tables[0];
            FarmaGV.DataBind();
            Con.Close();
        }
        private void DelFarm()
        {

            if (MName.Value == "" || MTip.SelectedIndex == -1 || Cantitate.Value == "" || Preț.Value == "" || Fabricat.SelectedIndex == -1)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Selectează un medicament pentru a-l ȘTERGE');", true);
                // eroare

            }
            else

            {
                try
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("Delete from FarmacieTB where Nr=@Mk", Con);
                    cmd.Parameters.AddWithValue("@Mk", FarmaGV.SelectedRow.Cells[1].Text);
                    cmd.ExecuteNonQuery();
                    Con.Close();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('S-au sters medicamente din stoc');", true);
                    ShowFarmacie();
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }
        private void EditFarm()
        {
            if (MName.Value == "" || MTip.SelectedIndex == -1 || Cantitate.Value == "" || Preț.Value == "" || Fabricat.SelectedIndex == -1)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Eroare');", true);
                // eroare
            }
            else

            {
                try
                {
                    Con.Open();
            SqlCommand cmd = new SqlCommand("update FarmacieTb set MNume=@MN,MTip=@mT, Cantitate=@C, Preț=@P, Fabricat=@MM where Nr=@Mk ", Con);
                    cmd.Parameters.AddWithValue("@MN", MName.Value);
                    cmd.Parameters.AddWithValue("@MT", MTip.SelectedItem.ToString());
                    cmd.Parameters.AddWithValue("@C", Cantitate.Value);
                    cmd.Parameters.AddWithValue("@P", Preț.Value);
                    cmd.Parameters.AddWithValue("@MM", Fabricat.SelectedIndex);
                    cmd.Parameters.AddWithValue("@Mk", FarmaGV.SelectedRow.Cells[1].Text);
                    cmd.ExecuteNonQuery();
                    Con.Close();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('S-au actualizat medicamente în stoc');", true);
                    ShowFarmacie();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        private void InsertFarm()
        {
            if (MName.Value == "" || MTip.SelectedIndex == -1 || Cantitate.Value == "" || Preț.Value == "" || Fabricat.SelectedIndex == -1)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Eroare');", true);
            }
            else

            {
                try
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("insert into FarmacieTb(MNume,MTip, Cantitate, Preț, Fabricat) values(@MN, @mT,@C,@P,@MM)", Con);
                    cmd.Parameters.AddWithValue("@MN", MName.Value);
                    cmd.Parameters.AddWithValue("@MT", MTip.SelectedItem.ToString());
                    cmd.Parameters.AddWithValue("@C", Cantitate.Value);
                    cmd.Parameters.AddWithValue("@P", Preț.Value);
                    cmd.Parameters.AddWithValue("@MM", Fabricat.SelectedIndex);
                    cmd.ExecuteNonQuery();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('S-au adăugat medicamente în stoc);", true);
                    Con.Close();
                    ShowFarmacie();
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowFarmacie();

        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {
            InsertFarm();
        }

        protected void Grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('Grid View Clicked');", true);
        }
        int k = 0;
        protected void FarmaGV_SelectedIndexChanged(object sender, EventArgs e)
        {
            MName.Value = FarmaGV.SelectedRow.Cells[2].Text;
            MTip.DataTextFormatString = FarmaGV.SelectedRow.Cells[3].Text;
            Cantitate.Value = FarmaGV.SelectedRow.Cells[4].Text;
            Preț.Value = FarmaGV.SelectedRow.Cells[5].Text;
            Fabricat.SelectedValue = FarmaGV.SelectedRow.Cells[6].Text;
            if (MName.Value == " ")
            {
                k = 0;
            }
            else
            {
                k = Convert.ToInt32(FarmaGV.SelectedRow.Cells[1].Text);
            }

        }


        protected void MTip_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Unnamed1_Click1(object sender, EventArgs e)
        {
            EditFarm();
        }

        protected void Unnamed1_Click2(object sender, EventArgs e)
        {
            DelFarm();
        }

        private void PrintFarmacie()
        {
            var printDoc = new PrintDocument();
            printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
            printDoc.Print();


        }
        private void PrintPage(object sender, PrintPageEventArgs e)
        {
            try
            {
                Graphics graphics = e.Graphics;
                Font fontTitle = new Font("Arial", 16, FontStyle.Bold);
                Font fontTableCell = new Font("Arial", 12);
                float x = e.MarginBounds.Left;
                float y = e.MarginBounds.Top;
                float cellPadding = 5;

                string title = "Detalii printate despre produsele introduse";
                
                // Afiseaza titlu si subtitlu
                graphics.DrawString(title, fontTitle, Brushes.Black, x, y);
                y += fontTitle.Height + cellPadding;

                Font fontSubtitle = new Font("Arial", 9, FontStyle.Regular);
                string subtitle = String.Format("Data: {0}", DateTime.Now);
                graphics.DrawString(subtitle, fontSubtitle, Brushes.Black, x, y);
                y += fontSubtitle.Height + cellPadding;


                // Desenarea capului de tabel
                float cellWidth = e.MarginBounds.Width / 5; // Lățimea celulei
                float cellHeight = fontTableCell.Height + cellPadding; // Înălțimea celulei

                // Desenarea antetului tabelului
                graphics.DrawString("Nume produs", fontTableCell, Brushes.Black, x, y);
                graphics.DrawString("Tip produs", fontTableCell, Brushes.Black, x + cellWidth, y);
                graphics.DrawString("Cantitate", fontTableCell, Brushes.Black, x + cellWidth * 2, y);
                graphics.DrawString("Preț", fontTableCell, Brushes.Black, x + cellWidth * 3, y);
                graphics.DrawString("Există pe raft?", fontTableCell, Brushes.Black, x + cellWidth * 4, y);
                y += cellHeight;

                // Desenarea fiecărui rând al tabelului
                foreach (GridViewRow row in FarmaGV.Rows)
                {
                    string numeProdus = row.Cells[2].Text;
                    string tipProdus = row.Cells[3].Text;
                    string cantitateProdus = row.Cells[4].Text;
                    string pretProdus = row.Cells[5].Text;
                    string fabricatProdus = row.Cells[6].Text;

                    graphics.DrawString(numeProdus, fontTableCell, Brushes.Black, x, y);
                    graphics.DrawString(tipProdus, fontTableCell, Brushes.Black, x + cellWidth, y);
                    graphics.DrawString(cantitateProdus, fontTableCell, Brushes.Black, x + cellWidth * 2, y);
                    graphics.DrawString(pretProdus, fontTableCell, Brushes.Black, x + cellWidth * 3, y);
                   
                    
                    y += cellHeight;
                }
            }
            catch (Exception ex)
            {
                // Afiseaza mesajul de eroare
 ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "alert('A apărut o eroare la imprimare: " + ex.Message + "');", true);
            }
        }


            protected void Printeaza_Click(object sender, EventArgs e)
        {
            PrintFarmacie();
        }
    }
}




