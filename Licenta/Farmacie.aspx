﻿<%@ Page Title="Stoc" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Farmacie.aspx.cs" Inherits="Licenta.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <html lane="en">
    <head>

       <title>Farma</title>

        <link rel="stylesheet" href="Farmacie.css">
        
        <style>
            .table-condesed tr th{
                border-style:outset;
                border-width: 5px;
                border-color: rgb(255, 216, 0);
            }
            .table-condesed, .table-condesed tr td{
                border: 0px solid #000;
            }
            tr:nth-child(even){
                background:rgb(255, 216, 0);
            }
            tr:nth-child(odd){
                background:rgb(235, 205, 36);
            }
        </style>
    </head>
    <body>

        <div id="BigMed">
             
           <div id="id">
              <h2><center>Stoc - PharmaInfo</center></h2>   <br><br>       
 
               <center><input type="text" name="" id="MName" placeholder="Nume Medicament" runat="server">

          <asp:DropDownList ID="MTip" runat="server" SelectedIndexChanged="MTip_SelectedChanged" AutoPostBack="True" OnSelectedIndexChanged="MTip_SelectedIndexChanged">
                    <asp:ListItem Text="Sirop" Value="AntibioticeIașiSirop"></asp:ListItem>
                    <asp:ListItem Text="Comprimate" Value="PhizerMedComprimate"></asp:ListItem>
                    <asp:ListItem Text="Injectabil" Value="Sanofi-AventisInjectabil"></asp:ListItem>
                    <asp:ListItem Text="Perfuzabil" Value="AstrazenecaPerfuzabil"></asp:ListItem>

                </asp:DropDownList>

                <input type="number" name="" id="Cantitate" placeholder="Cantitate" runat="server">
                <input type="number" name="" id="Preț" placeholder="Preț/bucată" runat="server">
                   <asp:DropDownList ID="Fabricat" runat="server" SelectedIndexChanged="Fabricat_SelectedChanged">
                    <asp:ListItem Text="Antibiotice Iași" Value="AntibioticeIași"></asp:ListItem>
                    <asp:ListItem Text="Phizer Medicamente" Value="PhizerMed"></asp:ListItem>
                    <asp:ListItem Text="Sanofi-Aventis" Value="Sanofi-Aventis"></asp:ListItem>
                    <asp:ListItem Text="Astrazeneca" Value="Astrazeneca"></asp:ListItem>
                </asp:DropDownList>
               </center>
                                            
               <center>  
                   <asp:Button id="butadd" runat="server" Text="Adaugă" OnClick="Unnamed1_Click" style="background-color:olivedrab;width:70px;height:30px;border-radius:9px;margin:45px 0"></asp:Button>                                    
                   <asp:Button runat="server" Text="Editează" Style="background-color: olivedrab; width: 70px; height: 30px; border-radius: 9px; margin: 45px 0" OnClick="Unnamed1_Click1"></asp:Button>  
                  <asp:Button runat="server" Text="Șterge" Style="background-color: olivedrab; width: 70px; height: 30px; border-radius: 9px; margin: 45px 0" OnClick="Unnamed1_Click2"></asp:Button>  
                 <asp:Button runat="server" Text="Printează stoc" Style="background-color: olivedrab; width: 100px; height: 30px; border-radius: 9px; margin: 45px 0" OnClick="Printeaza_Click"></asp:Button>

               
                <div id="GridMV">
                    <asp:GridView ID="FarmaGV" runat="server" Width="934px" CssClass="table table-condensed table-hover" OnSelectedIndexChanged="FarmaGV_SelectedIndexChanged" AutoGenerateSelectButton="True">
                    </asp:GridView>

                </div>
            </div>  
           </div>
       
               </body>
    </html>
</asp:Content>
